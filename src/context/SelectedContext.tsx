import React, { createContext, useState } from "react";

interface SelectProviderProps {
  children: React.ReactNode;
}
interface Selected {
  selectedOption: string;
  setSelectedOption: (option: string) => void;
}
const initialVal = {
  selectedOption: "",
  setSelectedOption: () => {},
};

export const SelectedContext = createContext<Selected>(initialVal);

export const SelectedProvider: React.FC<SelectProviderProps> = ({
  children,
}) => {
  const [selectedOption, setSelectedOption] = useState<string>(
    initialVal.selectedOption
  );
  return (
    <SelectedContext.Provider value={{ selectedOption, setSelectedOption }}>
      {children}
    </SelectedContext.Provider>
  );
};
