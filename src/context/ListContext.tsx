import React, { createContext, useState } from "react";
import { GenerateNumber } from "../utils/GenerateNumber";
interface ListProviderProps {
  children: React.ReactNode;
}
interface Data {
  id: string;
  name: string;
}
interface ListContext {
  list: Data[];
  addItem: (newItemName: string) => void;
}
const initialVal = {
  list: [
    { id: GenerateNumber(), name: "mahya" },
    { id: GenerateNumber(), name: "samira" },
    { id: GenerateNumber(), name: "sara" },
    { id: GenerateNumber(), name: "amir" },
    { id: GenerateNumber(), name: "mehdi" },
    { id: GenerateNumber(), name: "paniz" },
  ],
  addItem: () => {},
};

export const listContext = createContext<ListContext>(initialVal);

let id = 1;
export const ListProvider: React.FC<ListProviderProps> = ({ children }) => {
  const [list, setList] = useState<Data[]>(initialVal.list);

  const handleAddItem = (name: string) => {
    const nameExists = !!list.find((el) => el.name === name);
    if (nameExists || name.length === 0) return;
    setList([...list, { name, id: String(id++) }]);
  };

  return (
    <listContext.Provider value={{ list, addItem: handleAddItem }}>
      {children}
    </listContext.Provider>
  );
};
