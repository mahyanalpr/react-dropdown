import React, { useContext } from "react";
import { Select, Option } from "../components";
import { listContext } from "../context";

const Home: React.FC = () => {
  const { list } = useContext(listContext);
  return (
    <div className="container">
      <Select placeholder="please select ...">
        {list.map((item) => (
          <Option value={item.name} key={item.id}>
            {item.name}
          </Option>
        ))}
      </Select>
    </div>
  );
};

export default Home;
