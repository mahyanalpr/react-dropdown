import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { SelectedProvider, ListProvider } from "./context";
import Home from "./pages/Home";
import "./styles/main.scss";
const App: React.FC = () => {
  //ther is another approach to use context in router
  //you can use them above Routes tag
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <ListProvider>
              <SelectedProvider>
                <Home />
              </SelectedProvider>
            </ListProvider>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
