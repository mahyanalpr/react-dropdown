import React, { ReactNode, useState, useRef } from "react";
import useOnClickOutside from "../../hooks/useOnClickOutSide";
import Input from "./Input";

const Select: React.FC<{
  children: ReactNode;
  defualtValue?: string;
  placeholder?: string;
}> = ({ children, defualtValue, placeholder = "Choose an option" }) => {
  const selectDropContainerRef = useRef<HTMLDivElement>(null);
  const inputRef = useRef<HTMLInputElement>(null);
  const [showDropdown, setShowDropdown] = useState<boolean>(false);

  const clickOutsideHandler = (): void => {
    setShowDropdown(false);
  };
  useOnClickOutside(selectDropContainerRef, inputRef, clickOutsideHandler);

  const showHandler = (): void => {
    setShowDropdown(!showDropdown);
  };

  return (
    <div className="select" ref={selectDropContainerRef}>
      <Input
        showDropdown={showDropdown}
        placeholder={placeholder}
        showHandler={showHandler}
      />
      <ul
        onMouseDown={(e) => e.preventDefault()}
        className={
          showDropdown
            ? "select__options select__options--show"
            : "select__options select__options--hide"
        }
      >
        {children}
      </ul>
    </div>
  );
};

export default Select;
