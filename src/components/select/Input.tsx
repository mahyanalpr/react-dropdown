import React, { useContext, useState } from "react";
import { listContext, SelectedContext } from "../../context";
import down from "../../assets/img/down.png";
import up from "../../assets/img/up.png";

interface InputPropsType {
  showDropdown: boolean;
  placeholder: string;
  showHandler: () => void;
}

const Input: React.FC<InputPropsType> = ({
  showDropdown,
  showHandler,
  placeholder,
}) => {
  const { addItem } = useContext(listContext);
  const { selectedOption, setSelectedOption } = useContext(SelectedContext);
  const [userInput, setUserInput] = useState<string>("");

  const inputValue = userInput || selectedOption;

  const handleAddNewOption = (newOptionName: string) => {
    addItem(newOptionName);
    setSelectedOption(newOptionName);
    setUserInput("");
  };

  return (
    <div className="select__header">
      <img
        className="select__img"
        src={showDropdown ? up : down}
        alt="arrow-up"
      />
      <input
        type="text"
        className="select__input"
        placeholder={placeholder}
        value={inputValue}
        onChange={(e) => {
          setUserInput(e?.target?.value);
        }}
        onKeyDown={(e) => {
          if (e.key === "Enter") {
            handleAddNewOption(userInput);
          }
        }}
        onClick={showHandler}
      />
    </div>
  );
};

export default Input;
