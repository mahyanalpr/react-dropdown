import React, { ReactNode, useContext } from "react";
import { SelectedContext } from "../../context";
import selected from "../../assets/img/selected.png";

const Option: React.FC<{
  children: ReactNode | ReactNode[];
  value: string;
}> = ({ children, value }) => {
  const { setSelectedOption, selectedOption } = useContext(SelectedContext);
  return (
    <div
      className={
        selectedOption === value
          ? "select__option select__option--active"
          : "select__option"
      }
    >
      <li
        className="select__option-li"
        onMouseDown={(e) => e.preventDefault()}
        onClick={(e) => {
          setSelectedOption(value);
        }}
      >
        {children}
      </li>
      {selectedOption === value && (
        <img className="select__option-img" src={selected} alt="select-icon" />
      )}
    </div>
  );
};

export default Option;
